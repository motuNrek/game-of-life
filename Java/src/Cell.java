
/*
 * Diese Klasse könnte ohne Weiteres durch
 * die Klasse Point ersetzt werden. Sie beinhaltet
 * ausschließlich die Koordinaten der Zelle.
 */

public class Cell {
	int Row;
	int Column;
	
	public Cell(int y, int x){
		this.Row = y;
		this.Column = x;
	}
	
	public boolean equals(Object o){
	    if(o instanceof Cell){
	        Cell toCompare = (Cell) o;
	        return (this.Row == toCompare.Row && this.Column == toCompare.Column);
	    }
	    return false;
	}
}

import java.util.ArrayList;
import java.awt.Color;


public class Master {	
	public int H, B;														// Anzahl der Zeilen (H) und der Spalten (B)
	private Display display;
	
	private boolean[][] Feld, NeuesFeld;									// Das Feld, bzw. neue Feld nach dem Step: eine Zelle ist true, falls lebendig
	private ArrayList<Cell> CellsToCheck = new ArrayList<Cell>();			// Diese beiden Variablen sind bei der
	private ArrayList<Cell> ChangedCells = new ArrayList<Cell>();			// Funktion Step nötig, um an Arbeit zu sparen
	
	private Cell[][][] Around;												// beinhaltet für jede Zelle die 8 Zellen herum
	
	Color darkGreen = new Color(0,100,0);
	
	
	// Constructor
	Master(){
		this.display = new Display(this);
	}
	
	//--------------------------------------------------------
	
	/*
	 * Diese Funktion initialisiert alle notwendigen Werte.
	 * 		- H, B
	 * 		- Feld
	 * 		- neuesFeld
	 * 		- Around
	 */
	public void init(int h, int b){
		this.H = h;
		this.B = b;
		
		this.display.repaint();
		
		this.Feld = new boolean[h][b];
		this.NeuesFeld = new boolean[h][b];
		
		this.Around = new Cell[h][b][8];
		
		this.CellsToCheck.clear();
		this.ChangedCells.clear();		
		
		Cell[] around;
		int y,x,counter;
		int i = 0;
		int j,k,l;
		
		while (i < h){
			j = 0;
			while( j < b){
				
				around = new Cell[8];
				counter = 0;
				k = -1;
				while (k <= 1){
					l = -1;
					while (l <= 1){
						y = i + k;
						x = j + l;
						
						if (y < 0){
							y += h;
						}
						else if (y >= h){
							y -= h;
						}
						
						if (x < 0){
							x += b;
						}
						else if (x >= b){
							x -= b;
						}
						
						
						if (!(k == 0 && l == 0)){
							around[counter] = new Cell(y,x);
							counter += 1;
						}
						l += 1;
					}
					k += 1;
				}
				this.Around[i][j] = around;
				j += 1;
			}
			i += 1;
		}
	}
	
	//---------------------------------------------------------
	
	/*
	 * Diese Funktion zählt die lebendigen Zellen,
	 * die sich in Nachbarschaft zu "cell" befinden.
	 */
	private int countAround(Cell cell){
		int counter = 0;
		int y = cell.Row;
		int x = cell.Column;
		
		for (Cell check : this.Around[y][x]){
			if (this.cellAlive(check)){
				counter++;
			}
		}
		
		return counter;
	}
	
	
	/*
	 * Diese Funktion führt keine ganze Etappe, sondern
	 * nur eine halbe aus. D.h. dass es sich das alte Feld 
	 * merkt, allerdings bereits markiert, welche Zellen
	 * sterben werden (rot) und welche geboren werden
	 * (hellgrün). Die Farben stehen für die Visualisierung,
	 * wenn man auf dem Display "Step" angibt.
	 * 
	 * Aus Optimisierungsgründen werden nicht alle Zellen
	 * geprüft. Die Zellen die geprüft werden sind die, bei denen
	 * sich min. ein Nachbar in der Vorrunde verändert hat.
	 * Keine andere Zelle kann sich verändern.
	 */
	public boolean HalfStep(boolean state){
		int count;
		int y, x;

		
		if (!(state)){
			this.ChangedCells.clear();
			this.NeuesFeld = new boolean[this.H][this.B];
			
			ArrayList<Cell> newCellsToCheck = new ArrayList<Cell>();
			
			for (Cell cell : this.CellsToCheck){
				count = this.countAround(cell);
				
				y = cell.Row;
				x = cell.Column;

				if (this.cellAlive(cell)){
					
					if (count <= 1 || count >= 4){  // in diesem Fall stirbt die Zelle
						this.ChangedCells.add(cell);
						
						if (!(newCellsToCheck.contains(cell))) newCellsToCheck.add(cell);
						for (Cell iter : this.Around[y][x]){
							if (!(newCellsToCheck.contains(iter))) newCellsToCheck.add(iter);
						}
						this.display.colorCell(cell, Color.RED);
					}
					else {
						this.NeuesFeld[y][x] = true;
						if (!(newCellsToCheck.contains(cell))) newCellsToCheck.add(cell);
					}
				}
				else {
					if (count == 3){				// in diesem Fall wird die Zelle geboren
						
						this.NeuesFeld[y][x] = true;
						
						this.ChangedCells.add(cell);
						
						if (!(newCellsToCheck.contains(cell))) newCellsToCheck.add(cell);
						for (Cell iter : this.Around[y][x]){
							if (!(newCellsToCheck.contains(iter))) newCellsToCheck.add(iter);	
						}
						this.display.colorCell(cell, Color.GREEN);
					}
				}
			}
			
			this.CellsToCheck = new ArrayList<Cell>(newCellsToCheck);
		} 
		else {
			this.Feld = this.NeuesFeld.clone();
			
			for (Cell check : this.ChangedCells){
				if (this.cellAlive(check)){
					this.display.colorCell(check, this.darkGreen);
				}
				else {
					this.display.colorCell(check, Color.WHITE);
				}
			}
		}
		
		return !(state);
	}
	
	/*
	 * Diese Funktion führt die gesamte Etappe aus (und 
	 * benutzt dabei "Master.HalfStep".  Die Funktionsweise
	 * sonst ist aber relativ ähnlich.
	 */
	public void Step(){
		int count;
		int y,x;
		
		
		this.ChangedCells.clear();
		this.NeuesFeld = new boolean[this.H][this.B];
		
		ArrayList<Cell> newCellsToCheck = new ArrayList<Cell>();
		
		for (Cell cell : this.CellsToCheck){
			count = this.countAround(cell);
			
			y = cell.Row;
			x = cell.Column;

			if (this.cellAlive(cell)){

				y = cell.Row;
				x = cell.Column;
				
				if (count <= 1 || count >= 4){
					this.ChangedCells.add(cell);

					if (!(newCellsToCheck.contains(cell))) newCellsToCheck.add(cell);
					for (Cell iter : this.Around[y][x]){
						if (!(newCellsToCheck.contains(iter))){
							newCellsToCheck.add(iter);
						}	
					}
				}
				else {
					this.NeuesFeld[y][x] = true;
					if (!(newCellsToCheck.contains(cell))) newCellsToCheck.add(cell);
				}
			}
			else {
				if (count == 3){
					y = cell.Row;
					x = cell.Column;
					this.NeuesFeld[y][x] = true;
					
					this.ChangedCells.add(cell);
					
					if (!(newCellsToCheck.contains(cell))) newCellsToCheck.add(cell);
					for (Cell iter : this.Around[y][x]){
						if (!(newCellsToCheck.contains(iter))) newCellsToCheck.add(iter);
					}
				}
			}
		}
		this.CellsToCheck = new ArrayList<Cell>(newCellsToCheck);
		
		this.HalfStep(true);
	}
	
	//---------------------------------------------------------
	
	/*
	 * Funktion bestimmt, ob die Zelle "cell" lebendig ist.
	 */
	public boolean cellAlive(Cell cell){
		int y = cell.Row;
		int x = cell.Column;
		
		return this.Feld[y][x];
	}

	/*
	 * Aktiviert eine spezifische Zelle
	 */
	public void activateCell(Cell cell){
		int y = cell.Row;
		int x = cell.Column;
		
		if (!(this.CellsToCheck.contains(cell))){
			this.CellsToCheck.add(cell);
		}
		for (Cell iter : this.Around[y][x]){
			if (!(this.CellsToCheck.contains(iter))) this.CellsToCheck.add(iter);
		}
		
		this.Feld[y][x] = true;						// hier ist das tatsächliche Aktivieren
	}
	
	/*
	 * Tötet eine spezifische Zelle
	 */
	public void killCell(Cell cell){
		int y = cell.Row;
		int x = cell.Column;
		
		if (!(this.CellsToCheck.contains(cell))){
			this.CellsToCheck.add(cell);
			for (Cell iter : this.Around[y][x]){
				if (!(this.CellsToCheck.contains(iter))) this.CellsToCheck.add(iter);
			}
		}
		
		this.Feld[y][x] = false;					// hier findet das tatsächliche Abtöteten statt
	}
	
	// tötet alle Zellen.
	public void clearCells(){
		this.Feld = new boolean[this.H][this.B];
		this.CellsToCheck.clear();
	}
	
	//--------------------------------------------------------
	
	// Führt eine einmalige Initialisation durch, die am
	// Anfang obligatorisch durchgeführt werden muss.
	public void run(){
		this.display.setVisible(true);
		this.init(100, 199);
	}
}

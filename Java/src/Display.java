import javax.swing.Box;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JOptionPane;
import javax.swing.AbstractAction;
import javax.swing.KeyStroke;

import java.awt.Graphics;
import java.awt.Color;
import java.awt.Point;

import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;



@SuppressWarnings("serial")
public class Display extends JFrame{
	/*
	 * Diese Klasse beinhaltet die komplette GUI
	 * des Projekts, sowie die Funktionen, die die
	 * Rechenfunktionen von der Master-Klasse
	 * aufrufen.
	 * 
	 *  
	 * local classes:
	 * Canvas		: Kümmert sich um die visuelle 
	 * 					Darstellung der Zellen.
	 * SizeSelector : Kümmert sich um das Dialogfenster,
	 * 					mit dem der Benutzer die 
	 * 					Feldgröße individuell bestimmen kann.
	 * 
	 * Variablen:
	 * master				: Die Display-Klasse greift mit
	 * 							dieser Variable auf die
	 * 							Master-Klasse und deren
	 * 							Funktionen zu.
	 * figure				: Diese Variable speichert, sobald
	 * 							von Nöten, die Figur, die der
	 * 							Benutzer ausgewählt hat.
	 * 							(Möglicherweise muss sie noch
	 * 							volatile gestaltet werden.)
	 * canvas				: Zum malen.
	 * clearIsRunning		: Um zu überprüfen, ob gerade alle
	 * 							Zellen gelöscht werden.
	 * steppingIsRunning	: Zum Überprüfen, ob es Zwischenetappen
	 * 							gibt. (Vielleicht überflüssig?)
	 * sizeIsRunning		: Zum Überprüfen, ob gerade die Größe
	 * 							verändert wird.
	 * speedIsRunning		: Zum Überprüfen, ob gerade der
	 * 							Autoplay verändert wird.
	 * speedHasToStop		: Zum "soft stop" des Autoplays.
	 * speed				: Speichert die aktuelle Geschwindigkeit.
	 */
	
	private class Canvas extends JPanel implements MouseListener{
		/*
		 * Diese Klasse beschäftigt sich mit Allem, was das Visuelle
		 * angeht (Darstellung der Zellen, etc.), sowie mit Allem
		 * was die Interaktion zwischen Benutzer und GUI angeht
		 * (Anklicken, etc.).
		 * Allerdings gehört das MenuBar nicht zum Bereich des
		 * Canvas.
		 * 
		 * Variablen:
		 * rectWidth				: Breite eines "Zellenquadrats", hängt von
		 * 								der Anzahl der Zellen und der Breite
		 * 								(bzw. Höhe) des Fensters ab.
		 * mouseDown				: Gibt an, ob der Benutzer die Maus 
		 * 								gedrückt hält. Wird von den Funktionen
		 * 								Canvas.mousePressed und 
		 * 								Canvas.mouseReleased gesteuert.
		 * 								Sie wird von mehreren Threads 
		 * 								verändert, deshalb "volatile"
		 * paintRectanglesIsRunning	:
		 * 								Gibt an, ob das Programm gerade dabei
		 * 								ist, die angeklickten Quadrate 
		 * 								anzumalen oder nicht.
		 * stepIsRunning			: Gibt an, ob das Programm dabei
		 * 								ist, die Berechnung der nächsten
		 * 								Runde durchzuführen.
		 * state					: Gibt die Etappe an: Master.HalfStep(false)
		 * 								macht eine Zwischenrunde,
		 * 								Master.HalfStep(true) vollzieht
		 * 								dann die vorherige Zwischenrunde.
		 * stepping					: Gibt an, ob es Zwischenrunden gibt.
		 * darkGreen				: Ist ein dunkleres grün, um 
		 * 								verschieden Zustände auseinander halten zu können.
		 * 	
		 */
		private int rectWidth;
		
		volatile private boolean mouseDown = false;
		volatile private boolean paintRectanglesIsRunning = false;
		volatile private boolean stepIsRunning = false;
		volatile private boolean state = false;
		volatile private boolean stepping = true;
		
		Color darkGreen = new Color(0,100,0);
		
		Canvas(){
			/*
			 * Constructor
			 * 
			 * Allgemeine Dinge werden definiert:
			 * 
			 * StepAction ist eine anonyme Klasse, die
			 * durch getActionMap() an die Enter-Taste
			 * gebunden wird (vgl. getInputMap()).
			 * 
			 * Der MouseListener, der zu dem Frame hinzugefügt
			 * wird, kümmert sich dann um die Events,
			 * die der User durch das Anklicken der Fläche
			 * verursacht.
			 * 
			 * setFocusable(true) und requestFocus() 
			 * kümmern sich darum, dass auf das Hauptfenster
			 * fokussiert wird.
			 * 
			 * setBackground() kümmert sich um die 
			 * Hintergrundfarbe.
			 */
			AbstractAction StepAction = new AbstractAction(){
				/*
				 * Wenn diese Action aufgerufen wird, dann 
				 * wird makeStep() ausgeführt, sofern
				 * im Moment keine Step ausgeführt wird.
				 * Falls der Autoplay aktiviert ist, wird
				 * die Aktion temporär deaktiviert.
				 */
				@Override
				public void actionPerformed(ActionEvent e) {
					if (Canvas.this.checkStep() && !(Display.this.speedIsRunning)){
						new Thread(){
							public void run(){
								Canvas.this.makeStep();
								Canvas.this.stepIsRunning = false;
							}
						}.start();
					}
				}
			};
			
			this.addMouseListener(this);
			this.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "StepKey");
			this.getActionMap().put("StepKey", StepAction);
			
			this.setFocusable(true);
			this.requestFocus();
			
			this.setBackground(Color.WHITE);
		}
		
		public void paintComponent( Graphics g ){
			/*
			 * Diese Funktion wird automatisch aufgerufen,
			 * wenn JFrame.setVisible(true) ausgeführt wird,
			 * kann aber auch manuell aufgerufen werden.
			 * 
			 * Da sie bei jedem init der Master-Klasse
			 * aufgerufen wird, kann man vielleicht
			 * auch eine Funktion schreiben, die nicht
			 * automatisch ausgeführt wird.
			 */
			super.paintComponent(g);
			
			int height = this.getHeight();
			int width = this.getWidth();
			
			int h = Display.this.master.H;
			int b = Display.this.master.B;
			
			if (b == 0 || h == 0){
				System.out.println("Zero Division!!!!!!!");
				System.exit(0);
			}
			
			// Diese Variable könnte vielleicht auch direkt im 
			// init definiert werden.
			this.rectWidth = (int)(Math.min(width/b, height/h));
			
			for (int i=0; i<b; i++){
				for (int j=0; j<h; j++){
					g.drawRect(i*this.rectWidth, j*this.rectWidth, this.rectWidth, this.rectWidth);
				}
			}
		}
		
		//--------------------------------------------------------
		
		
		/*
		 * die synchronized Funktionen dienen dazu,
		 * volatile Variablen abzufragen, die 
		 * ständig von mehreren Threads benutzt werden.
		 */
		
		private synchronized boolean checkPaintRectangles(){
			if (this.paintRectanglesIsRunning){
				return false;
			}
			this.paintRectanglesIsRunning = true;
			return true;
		}
		
		public synchronized boolean checkStep(){
			if (this.stepIsRunning){
				return false;
			}
			this.stepIsRunning = true;
			return true;
		}
		
		public synchronized boolean checkStepping(){return this.stepping;}
		
		public synchronized boolean checkState(){return this.state;}
		
		//--------------------------------------------------------
		
		private void initThreadPaintRectangles(){
			/*
			 * Diese Funktion wird aufgerufen, wenn der
			 * Benutzer Quadrate anklickt, um die 
			 * entsprechenden Zellen zu "beleben"
			 * und anzumalen.
			 */
			if (this.checkPaintRectangles()){
				new Thread(){
					public void run(){
						if (Canvas.this.checkState()){
							Canvas.this.state = Display.this.master.HalfStep(true);
						}
						
						Graphics g = Canvas.this.getGraphics();

						int rectWidth = Canvas.this.rectWidth;
						/*
						 * Diese zwei variablen sind dazu da, dass
						 * durch die Schleifen nicht ein Feld
						 * ständig aus und an geht, weil sich
						 * der Benutzer auf dem gleichen Feld
						 * befindet (also flimmert).
						 */
						int lastj = -1;
						int lasti = -1;
						int i,j;
						
						do {
							Point event = Canvas.this.getMousePosition();
							
							try {
								j = (int)event.getX()/Canvas.this.rectWidth;
								i = (int)event.getY()/Canvas.this.rectWidth;
							} catch (NullPointerException e){
								Display.this.requestFocus(); //Macht trotzdem noch Probleme
								return;
							}
							
							if (!(j == lastj && i == lasti)){
								
								int h = Display.this.master.H;
								int b = Display.this.master.B;
								
								if (j > b || i > h){
									return;
								}
								
								Cell cell = new Cell(i,j);
								
								boolean alive = Display.this.master.cellAlive(cell);
								
								if (alive){
									g.setColor(Color.WHITE);
									Display.this.master.killCell(cell);
								}
								else {
									g.setColor(Canvas.this.darkGreen);
									Display.this.master.activateCell(cell);
								}
								
								g.fillRect(j*rectWidth+1, i*rectWidth+1, rectWidth-1, rectWidth-1);
								g.setColor(Color.BLACK);
								
								lastj = j;
								lasti = i;
							}
						} while (Canvas.this.mouseDown);
						Canvas.this.paintRectanglesIsRunning = false;
					}
				}.start();
			}
		}
		
		private void makeStep(){
			/*
			 * Diese Funktion kümmert sich um
			 * das eigentliche Ausführen der 
			 * Step-Funktionen.
			 */
			if (this.checkStepping()){
				if (this.checkState()){
					this.state = Display.this.master.HalfStep(true); // this.state = Display.this.master.HalfStep(this.state); 
				}
				else {
					this.state = Display.this.master.HalfStep(false);
				}
			}
			else {
				Display.this.master.Step();
			}
		}
		
		//--------------------------------------------------------
		
		public void mousePressed(MouseEvent event){
			
			if (event.getButton() == MouseEvent.BUTTON1){
				this.mouseDown = true;
				if (!(Display.this.speedIsRunning)) this.initThreadPaintRectangles();
			}
		}
		
		public void mouseReleased(MouseEvent e){
			if (e.getButton() == MouseEvent.BUTTON1){
				this.mouseDown = false;
			}
		}
		
		/*
		 * Diese Funktionen MÜSSEN implementiert 
		 * sein, haben bei uns aber keine Funktion.
		 */
		public void mouseEntered(MouseEvent e){}
		public void mouseExited(MouseEvent e){}
		public void mouseClicked(MouseEvent e){}
		
	}
	
	private class SizeSelector extends JFrame {
		/*
		 * Hier sollte man das vielleicht anders machen, es ist bie jetzt 
		 * noch nicht wirklich zufriedenstellend, aber für den Moment
		 * reicht es.
		 * 
		 * Beispielsweise indem man einfach einen SizeSelector
		 * selber programmiert, anstatt JPanel zu benutzen.
		 */
		JTextField yField, xField;
		JPanel myPanel;
		
		SizeSelector (JFrame frame){
			this.yField = new JTextField(5);
			this.xField = new JTextField(5);
			
			this.yField.setFocusable(true);

		    this.myPanel = new JPanel();
		    this.myPanel.add(new JLabel("Rows:"));
		    this.myPanel.add(this.yField);
		    this.myPanel.add(Box.createHorizontalStrut(15)); // a spacer
		    this.myPanel.add(new JLabel("Columns:"));
		    this.myPanel.add(this.xField);
		}
		
		public String[] run(){
			int result = JOptionPane.showConfirmDialog(null, this.myPanel, 
		    		"Please enter size of the grid.", JOptionPane.OK_CANCEL_OPTION);
			this.yField.requestFocus();
			
			String ret[] = {"", ""};
			
		    if (result == JOptionPane.OK_OPTION) {
		    	String y = this.yField.getText();
		    	String x = this.xField.getText();
		    	
		    	ret[0] = y;
		    	ret[1] = x;
		    }
		    else if (result == JOptionPane.CANCEL_OPTION){
		    	ret[0] = "Cancel";
		    	ret[1] = "Cancel";
		    }
		    else if (result == JOptionPane.CLOSED_OPTION){
		    	ret[0] = "Cancel";
		    	ret[1] = "Cancel";
		    }
		    return ret;
		}
		
	}
	
	private Master master;
	private String figure;
	private Canvas canvas;
	
	volatile private boolean clearIsRunning = false;
	volatile private boolean steppingIsRunning = false;
	volatile private boolean sizeIsRunning = false;
	volatile private boolean speedIsRunning = false;
	volatile private boolean speedHasToStop = false;
	
	int speed;
	
	
	Display(Master master){
		/*
		 * Constructor
		 * 
		 * Generelle Einstellung:
		 * 		- Größe
		 * 		- Titel
		 * 		- Was passiert wenn geschlossen wird?
		 * 		- Canvas initialisierung
		 * 		- Menubar
		 * 		- Menus
		 * 		- Menuitems (mit bindings)
		 */
		this.master = master;
		
		this.setSize(1600, 870);
		this.setTitle("Conway's Game Of Life");
		this.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
		
		
		this.canvas = new Canvas();
		this.add(this.canvas);
		this.setLocationRelativeTo(null);
		
		
		JMenuBar Menubar;
		JMenu SpeedMenu, SizeMenu, FigureMenu, OptionsMenu, HelpMenu;
		
		Menubar = new JMenuBar();
		
		SpeedMenu = new JMenu("Speed");
		JMenuItem stopitem = new JMenuItem("Stop");
		SpeedMenu.add(stopitem);
		JMenuItem slowitem = new JMenuItem("Slow");
		SpeedMenu.add(slowitem);
		JMenuItem mediumitem = new JMenuItem("Medium");
		SpeedMenu.add(mediumitem);
		JMenuItem fastitem = new JMenuItem("Fast");
		SpeedMenu.add(fastitem);
		
		stopitem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SpeedSelected(0);
			}
		});
		slowitem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SpeedSelected(1);
			}
		});
		
		mediumitem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SpeedSelected(2);
			}
		});
		fastitem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SpeedSelected(3);
			}
		});
		
		Menubar.add(SpeedMenu);
		
		SizeMenu = new JMenu("Size");
		JMenuItem tentenitem = new JMenuItem("10x10");
		JMenuItem twentwentitem = new JMenuItem("20x20");
		JMenuItem dreidreiitem = new JMenuItem("30x30");
		JMenuItem zweisechsitem = new JMenuItem("20x60");
		JMenuItem otheritem = new JMenuItem("other...");
		SizeMenu.add(tentenitem);
		SizeMenu.add(twentwentitem);
		SizeMenu.add(dreidreiitem);
		SizeMenu.add(zweisechsitem);
		SizeMenu.add(otheritem);
		
		tentenitem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (Display.this.checkSize()){
					new Thread(){
						public void run(){
							Display.this.SizeSelected(10,10);
							Display.this.sizeIsRunning = false;
						}
					}.start();
				}
			}
		});
		twentwentitem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (Display.this.checkSize()){
					new Thread(){
						public void run(){
							Display.this.SizeSelected(20,20);
							Display.this.sizeIsRunning = false;
						}
					}.start();
				}
			}
		});
		dreidreiitem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (Display.this.checkSize()){
					new Thread(){
						public void run(){
							Display.this.SizeSelected(30,30);
							Display.this.sizeIsRunning = false;
						}
					}.start();
				}
			}
		});
		zweisechsitem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (Display.this.checkSize()){
					new Thread(){
						public void run(){
							Display.this.SizeSelected(20,60);
							Display.this.sizeIsRunning = false;
						}
					}.start();
				}
			}
		});
		otheritem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SizeSelector dialog = new SizeSelector(Display.this);
				
				String result[] = dialog.run();
				int rows, columns;
				
				if (result[0].length() == 0 || result[1].length() == 0){
					JOptionPane.showMessageDialog(Display.this, "You have to enter Integers", "TypeError", JOptionPane.ERROR_MESSAGE);
				}
				else {
					if (!(result[0] == "Cancel" && result[1] == "Cancel")){
						try {
							rows = Integer.parseInt(result[0]);
							columns = Integer.parseInt(result[1]);
							
							if (rows <= -1 || columns <= -1){
								JOptionPane.showMessageDialog(Display.this, "You have to enter positif numbers", "ValueError", JOptionPane.ERROR_MESSAGE);
							} else {
								Display.this.master.init(rows, columns);
							}
						} catch (NumberFormatException e1){
							JOptionPane.showMessageDialog(Display.this, "You have to enter Integers", "TypeError", JOptionPane.ERROR_MESSAGE);
						}
					}
				}
				
			}
		});
		
		Menubar.add(SizeMenu);
		
		FigureMenu = new JMenu("Figures");
		/*
		 * 
		 * Hier kommen noch die ganzen Optionen für
		 * die unterschiedlichen Figuren hin. Aber
		 * das machen wir später.
		 * 
		 */
		Menubar.add(FigureMenu);
		
		OptionsMenu = new JMenu("Options...");
		JMenuItem clearitem = new JMenuItem("Clear");
		JMenuItem stepitem = new JMenuItem("Step");
		OptionsMenu.add(clearitem);
		OptionsMenu.add(stepitem);
		
		clearitem.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if (Display.this.checkClear()){
					
					new Thread(){
						public void run(){
							Display.this.master.clearCells();
							Display.this.SpeedSelected(0);
							Display.this.clearIsRunning = false;
						}
					}.start();
					

					Graphics g = Display.this.canvas.getGraphics();
					Display.this.canvas.paintComponent(g);
				}
			}
		});
		stepitem.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if (Display.this.checkStepping()){
					new Thread(){
						public void run(){
							if (Display.this.canvas.checkStepping()){
								if (Display.this.canvas.checkState()){
									Display.this.canvas.makeStep();
								}
								Display.this.canvas.stepping = false;
							}
							else {
								Display.this.canvas.stepping = true;
							}
							Display.this.steppingIsRunning = false;
						}
					}.start();
				}
			}
		});
		
		Menubar.add(OptionsMenu);
		
		HelpMenu = new JMenu("Help");
		JMenuItem exititem = new JMenuItem("Exit");
		//
		HelpMenu.add(exititem);
		
		exititem.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				System.exit(0);
			}
		});
		
		Menubar.add(HelpMenu);
		
		
		this.setJMenuBar(Menubar);
	}
	
	//-------------------------------------------------------------
	
	public void repaint(){
		/*
		 * Wird vom Master aufgerufen, um das Spielfeld
		 * neuzumachen.
		 */
		Graphics g = this.canvas.getGraphics();
		this.canvas.paintComponent(g);
	}
	
	public void colorCell(Cell cell, Color color){
		/*
		 * Wird vom Master aufgerufen, um eine einzelne
		 * Zelle zu bemalen.
		 */
		Graphics g = this.canvas.getGraphics(); 
		
		int y = cell.Row;
		int x = cell.Column;
		
		int h = this.master.H;
		int b = this.master.B;
		
		if (x > b || y > h){
			return;
		}

		g.setColor(color);
		g.fillRect(x*this.canvas.rectWidth+1, y*this.canvas.rectWidth+1, this.canvas.rectWidth-1, this.canvas.rectWidth-1);
		g.setColor(Color.BLACK);
	}
	
	//-------------------------------------------------------------
	
	
	/*
	 * siehe Canvas
	 */
	private synchronized boolean checkClear(){
		if (this.clearIsRunning){
			return false;
		}
		this.clearIsRunning = true;
		return true;
	}
	
	private synchronized boolean checkStepping(){
		if (this.steppingIsRunning){
			return false;
		}
		this.steppingIsRunning = true;
		return true;
	}
	
	private synchronized boolean checkSize(){
		if (this.sizeIsRunning){
			return false;
		}
		this.sizeIsRunning = true;
		return true;
	}
	
	private synchronized boolean checkSpeed(){
		if (this.speedIsRunning){
			return false;
		}
		this.speedIsRunning = true;
		return true;
	}
	
	//--------------------------------------------------------------
	
	private void SizeSelected(int rows, int columns){
		/*
		 * Ruft die Neuinitialisierung der Master-Klasse auf
		 * (mit den entsprechend geänderten Größen.
		 */
		if (!(rows == this.master.H && columns == this.master.B)){
			this.master.init(rows, columns);
		}
	}
	
	private void SpeedSelected(int speed){
		/*
		 * Diese Funktion kümmert sich um den Autoplay.
		 * Wird eine andere Geschwindigkeit angegeben,
		 * muss ggf. ein neuer Thread geöffnet werden
		 * (und der alte SAUBER geshclossen werden).
		 */
		this.speed = speed;
		
		if (this.checkSpeed()){
			if (speed == 0) {
				this.speedIsRunning = false;
			} else {
				new Thread(){
					public void run(){
						int time = 300/Display.this.speed;
						
						while (Display.this.canvas.stepIsRunning);
						if (Display.this.canvas.stepping){
							if (Display.this.canvas.state){
								Display.this.canvas.makeStep();
							}
						}
						Display.this.canvas.stepping = false;
						
						while (!(Display.this.speedHasToStop)){
							Display.this.canvas.makeStep();
							try{
								Thread.sleep((long)time);
							} catch (InterruptedException e){
								Thread.currentThread().interrupt();
							}
						} 
						Display.this.speedIsRunning = false;
						Display.this.speedHasToStop = false;
					}
				}.start();
			}
		} else {
			this.speedHasToStop = true;
			while (this.speedHasToStop || this.speedIsRunning);
			
			if (!(speed == 0)){
				this.speedIsRunning = true;
				new Thread(){
					public void run(){
						int time = 300/Display.this.speed;
						
						while (Display.this.canvas.stepIsRunning);
						if (Display.this.canvas.stepping){
							if (Display.this.canvas.state){
								Display.this.canvas.makeStep();
							}
						}
						Display.this.canvas.stepping = false;
						
						while (!(Display.this.speedHasToStop)){
							Display.this.canvas.makeStep();
							try{
								Thread.sleep((long)time);
							} catch (InterruptedException e){
								Thread.currentThread().interrupt();
							}
						}
						Display.this.speedIsRunning = false;
						Display.this.speedHasToStop = false;
					}
				}.start();
			}
		}
	}
}


public class Run {

	/*
	 * Cette classe est l'application finale
	 * qui est réellement exécutée.
	 */
	public static void main(String[] args) {
		Master master = new Master();
		master.run();

	}

}

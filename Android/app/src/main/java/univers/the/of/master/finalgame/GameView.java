package univers.the.of.master.finalgame;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.view.View;


import android.util.Log;


/**
 * Created by julian on 15.03.16.
 */
public class GameView extends View{
    final int darkGreen = new Color().rgb(0,100,0);

    private Paint paint;
    private Canvas canvas;
    private Bitmap cache;

    private Calc Master;

    private int rectWidth;
    public volatile Point lastPosition = new Point(-1, -1);
    private volatile boolean isInit = false;


    // Constructor
    public GameView(Calc master, Context context){
        super(context);

        this.paint = new Paint();
        this.canvas = new Canvas();

        this.Master = master;
    }


    // draws on screen
    @Override
    public void onDraw(Canvas canvas){
        canvas.drawBitmap(this.cache,
                new Rect(0, 0, this.cache.getWidth(), this.cache.getHeight()),
                new Rect(0, 0, this.cache.getWidth(), this.cache.getHeight()), null);

        if (!(this.isInit)) this.isInit = true;
    }


    //<---------------- auxiliary functions ---------------------->

    private void fillRect(int x, int y, int color){
        this.paint.setStyle(Paint.Style.FILL);
        this.paint.setColor(color);

        this.canvas.drawRect(x + 1, y + 1,
                x + this.rectWidth, y + this.rectWidth,
                this.paint);
    }


    //<---------------- public functions ------------------------->

    public void init(){
        int w = this.Master.DisplayWidth;
        int h = this.Master.DisplayHeight;

        this.cache = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        this.canvas.setBitmap(this.cache);

        int H = this.Master.H;
        int B = this.Master.B;

        this.paint.setColor(Color.BLACK);
        this.paint.setStyle(Paint.Style.STROKE);
        this.paint.setStrokeWidth(1);

        this.rectWidth = (Math.min(w/B, h/H));

        for (int i=0; i<B; i++){
            for (int j=0; j<H; j++){
                this.canvas.drawRect(i * this.rectWidth, j * this.rectWidth,
                        (i + 1) * this.rectWidth, (j + 1) * this.rectWidth,
                        paint);
            }
        }
        postInvalidate();
    }


    public void colorCell(Point cell, int color){
        int row = cell.x * this.rectWidth;
        int column = cell.y * this.rectWidth;

        this.fillRect(column, row, color);
        postInvalidate();
    }

    public void manipulateGrid(int xEvent, int yEvent){
        int i = (xEvent/this.rectWidth);
        int j = (yEvent/this.rectWidth);

        if (i != this.lastPosition.x || j != this.lastPosition.y){
            lastPosition.x = i;
            lastPosition.y = j;

            if (i >= this.Master.B || j >= this.Master.H) return;
            if (this.cache == null){
                Log.d("Warning:", "NullPointerException raised in GameView.manipulateGrid(int xEvent, int yEvent).");
                System.exit(0);
            }

            Point cell = new Point(j,i);
            i *= this.rectWidth;
            j *= this.rectWidth;

            if (this.Master.cellAlive(cell)){
                this.Master.killCell(cell);
                this.fillRect(i,j,Color.WHITE);
            } else {
                this.Master.activateCell(cell);
                this.fillRect(i,j,this.darkGreen);
            }

            postInvalidate();

        }
    }
}

package univers.the.of.master.finalgame;


import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;

/**
 * Created by julian on 15.03.16.
 */
public class Calc {
    public int H=10, B=10;

    public int DisplayWidth, DisplayHeight;
    public GameView gameView;

    final int darkgreen = new Color().rgb(0, 100, 0);

    private boolean[][] Field, newField;
    private ArrayList<Point> CellsToCheck = new ArrayList<>();
    private ArrayList<Point> ChangedCells = new ArrayList<>();
    private Point[][][] Around;


    // Constructor
    Calc(int width, int height, Context context){
        this.DisplayHeight = height;
        this.DisplayWidth = width;

        this.gameView = new GameView(this, context);
        this.gameView.setBackgroundColor(Color.WHITE);
    }


    //<------------------- auxiliary functions ---------------------->

    public boolean cellAlive(Point cell){
        return this.Field[cell.x][cell.y];
    }

    private int countAround(Point cell){
        int counter = 0;

        int x = cell.x;
        int y = cell.y;

        for (Point check : this.Around[x][y]){
            if (this.cellAlive(check)){
                counter++;
            }
        }

        return counter;
    }

    private boolean HalfStep(boolean state){
        int count;
        int x, y;

        if (!(state)){
            this.ChangedCells.clear();
            this.newField = new boolean[this.H][this.B];

            ArrayList<Point> newCellsToCheck = new ArrayList<>();

            for (Point cell : this.CellsToCheck){
                count = this.countAround(cell);

                if (this.cellAlive(cell)){
                    x = cell.x;
                    y = cell.y;

                    if (count <= 1 || count >= 4){
                        this.ChangedCells.add(cell);

                        if (!(newCellsToCheck.contains(cell))){
                            newCellsToCheck.add(cell);
                        }
                        for (Point iter : this.Around[x][y]){
                            if (!(newCellsToCheck.contains(iter))){
                                newCellsToCheck.add(iter);
                            }
                        }

                        this.gameView.colorCell(cell, Color.RED);
                    }
                    else {
                        this.newField[x][y] = true;
                        if (!(newCellsToCheck.contains(cell))){
                            newCellsToCheck.add(cell);
                        }
                    }
                }
                else {
                    if (count == 3){
                        x = cell.x;
                        y = cell.y;

                        this.newField[x][y] = true;
                        this.ChangedCells.add(cell);

                        if (!(newCellsToCheck.contains(cell))){
                            newCellsToCheck.add(cell);
                        }
                        for (Point iter : this.Around[x][y]){
                            if (!(newCellsToCheck.contains(iter))){
                                newCellsToCheck.add(iter);
                            }
                        }

                        this.gameView.colorCell(cell, Color.GREEN);
                    }
                }
            }

            this.CellsToCheck = new ArrayList<>(newCellsToCheck);
        }
        else {
            this.Field = this.newField.clone();

            for (Point check : this.ChangedCells){
                if (this.cellAlive(check)){
                    this.gameView.colorCell(check, this.darkgreen);
                }
                else {
                    this.gameView.colorCell(check, Color.WHITE);
                }
            }
        }

        return !(state);
    }


    //<-------------------- public functions ------------------------>

    public void init(int h, int b){
        this.H = h;
        this.B = b;

        this.gameView.init();

        this.Field = new boolean[h][b];
        this.newField = new boolean[h][b];

        this.Around = new Point[h][b][8];

        this.CellsToCheck.clear();
        this.ChangedCells.clear();


        Point[] around;
        int x, y, counter;
        int i = 0;
        int j, k, l;

        while (i < h){
            j = 0;
            while (j < b){

                around = new Point[8];
                counter = 0;

                k = -1;
                while (k <= 1){
                    l = -1;
                    while (l <= 1){
                        x = (i + k);
                        y = (j + l);

                        if (x < 0) {
                            x += h;
                        } else if (x >= h) {
                            x -= h;
                        }

                        if (y < 0) {
                            y += b;
                        } else if (y >= b) {
                            y -= b;
                        }

                        if (!(k == 0 && l == 0)){
                            around[counter] = new Point(x, y);
                            counter++;
                        }
                        l++;
                    }
                    k++;
                }

                this.Around[i][j] = around;
                j++;
            }
            i++;
        }
    }

    public void Step(){
        int count;
        int x, y;

        this.ChangedCells.clear();
        this.newField = new boolean[this.H][this.B];

        ArrayList<Point> newCellsToCheck = new ArrayList<>();

        for (Point cell : this.CellsToCheck){
            count = this.countAround(cell);

            x = cell.x;
            y = cell.y;

            if (this.cellAlive(cell)){
                if (count <= 1 || count >= 4){
                    this.ChangedCells.add(cell);

                    if (!(newCellsToCheck.contains(cell))){
                        newCellsToCheck.add(cell);
                    }
                    for (Point iter : this.Around[x][y]){
                        if (!(newCellsToCheck.contains(iter))){
                            newCellsToCheck.add(iter);
                        }
                    }
                }
                else {
                    this.newField[x][y] = true;
                    if (!(newCellsToCheck.contains(cell))){
                        newCellsToCheck.add(cell);
                    }
                }
            }
            else {
                if (count == 3){
                    this.newField[x][y] = true;

                    this.ChangedCells.add(cell);

                    if (!(newCellsToCheck.contains(cell))){
                        newCellsToCheck.add(cell);
                    }
                    for (Point iter : this.Around[x][y]){
                        if (!(newCellsToCheck.contains(iter))){
                            newCellsToCheck.add(iter);
                        }
                    }
                }
            }
        }

        this.CellsToCheck = new ArrayList<>(newCellsToCheck);

        this.HalfStep(true);
    }

    public void activateCell(Point cell){
        int x = cell.x;
        int y = cell.y;

        if (!(this.CellsToCheck.contains(cell))){
            this.CellsToCheck.add(cell);
        }
        for (Point iter : this.Around[x][y]){
            if (!(this.CellsToCheck.contains(iter))){
                this.CellsToCheck.add(iter);
            }
        }

        this.Field[x][y] = true;
    }

    public void killCell(Point cell){
        int x = cell.x;
        int y = cell.y;

        if (!(this.CellsToCheck.contains(cell))){
            this.CellsToCheck.add(cell);
        }
        for (Point iter : this.Around[x][y]){
            if (!(this.CellsToCheck.contains(iter))){
                this.CellsToCheck.add(iter);
            }
        }

        this.Field[x][y] = false;
    }


    //<-------------------- main function --------------------------->

    public void run(){
        this.init(43, 26);
    }
}

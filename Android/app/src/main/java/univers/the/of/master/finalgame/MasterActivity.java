package univers.the.of.master.finalgame;



import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.*;
import android.view.Display;
import android.widget.EditText;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


public class MasterActivity extends AppCompatActivity{
    Calc Master;

    private volatile boolean stop = true;
    private volatile int playSpeed = 2;         // possible values : {2, 3, 4}
    private volatile boolean hasToStop = false;

    private volatile CharSequence actualSize = "100x60";

    private Menu menu;



    // when application starts
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        this.Master = new Calc(size.x, size.y-100, this);
        this.Master.run();


        // Fullscreen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        // Game Layout : displays gameView
        this.setContentView(this.Master.gameView);


        // GestureDetector; idea and basic code comes from
        // http://stackoverflow.com/questions/14506540/double-tap-event-in-an-activity
        final Context context = this;
        final GestureDetector.SimpleOnGestureListener listener = new GestureDetector.SimpleOnGestureListener(){

            // Cette méthode est appelée si l'utilisateur tape une seule
            // fois sur l'écran.
            @Override
            public boolean onSingleTapUp(MotionEvent e){
                int x = (int) e.getX();
                int y = (int) e.getY() - 100;       // cette correction est nécessaire à cause de la "toolbar" en haut

                if (MasterActivity.this.stop) {
                    MasterActivity.this.Master.gameView.manipulateGrid(x, y);
                }
                return true;
            }
        };
        final GestureDetector detector = new GestureDetector(listener);

        detector.setOnDoubleTapListener(listener);
        detector.setIsLongpressEnabled(true);

        getWindow().getDecorView().setOnTouchListener(new View.OnTouchListener(){
            @Override
            public boolean onTouch(View view, MotionEvent event){
                return detector.onTouchEvent(event);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_game, menu);

        this.menu = menu;
        return true;
    }

    /*
    Cette méthode est la méthode la plus complexe de toute la classe.
    Cependant, cette complexité n'est que visuelle. La classe fait
    plusieurs fois la même chose: différencier les actions utilisées
    si un utilisateur choisi dans le menu. Il y a cette répétition,
    puisque certains items ouvrent de nouvelles menus.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();


        if (id == R.id.action_settings) {
            MenuItem play_item = this.menu.findItem(R.id.action_play);
            String name = (String) play_item.getTitle();
            if (name == "Pause") {
                this.onOptionsItemSelected(play_item);
            }
            final AlertDialog levelDialog;
            final CharSequence[] settings = {"Select Size", "Select Speed", "About"};

            final AlertDialog.Builder settingsBuilder = new AlertDialog.Builder(this);
            settingsBuilder.setTitle("Settings");
            settingsBuilder.setSingleChoiceItems(settings, -1, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    switch (item) {
                        case 0:
                            dialog.cancel();

                            final AlertDialog secondLevel;

                            final CharSequence[] sizes = {"10x6", "20x12", "50x30", "100x60", "other"};

                            AlertDialog.Builder sizeBuilder = new AlertDialog.Builder(MasterActivity.this);
                            sizeBuilder.setTitle("Options");
                            sizeBuilder.setSingleChoiceItems(sizes, -1, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int item) {
                                    switch (item) {
                                        case 0:
                                            if (!(MasterActivity.this.actualSize == "10x6")) {
                                                MasterActivity.this.Master.init(10, 6);
                                                MasterActivity.this.actualSize = "10x6";
                                            }
                                            break;
                                        case 1:
                                            if (!(MasterActivity.this.actualSize == "20x12")) {
                                                MasterActivity.this.Master.init(20, 12);
                                                MasterActivity.this.actualSize = "20x12";
                                            }
                                            break;
                                        case 2:
                                            if (!(MasterActivity.this.actualSize == "50x30")) {
                                                MasterActivity.this.Master.init(50, 30);
                                                MasterActivity.this.actualSize = "50x30";
                                            }
                                            break;
                                        case 3:
                                            if (!(MasterActivity.this.actualSize == "100x60")) {
                                                MasterActivity.this.Master.init(100, 60);
                                                MasterActivity.this.actualSize = "100x60";
                                            }
                                            break;
                                        case 4:
                                            AlertDialog.Builder builder = new AlertDialog.Builder(MasterActivity.this);
                                            LayoutInflater inflater = MasterActivity.this.getLayoutInflater();


                                            builder.setView(inflater.inflate(R.layout.other_size__dialog, null))

                                                    .setPositiveButton(R.string.apply, new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialog, int id) {
                                                            Dialog f = (Dialog) dialog;

                                                            try {
                                                                int height = Integer.valueOf(((EditText) f.findViewById(R.id.height)).getText().toString());
                                                                int width = Integer.valueOf(((EditText) f.findViewById(R.id.width)).getText().toString());

                                                                MasterActivity.this.Master.init(height, width);
                                                                MasterActivity.this.actualSize = "" + height + "x" + width;
                                                            } catch (Exception e) {
                                                                Toast.makeText(MasterActivity.this, "Error occured :(", Toast.LENGTH_SHORT).show();
                                                                return;
                                                            }
                                                        }
                                                    })
                                                    .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog, int id) {
                                                            dialog.cancel();
                                                        }
                                                    });
                                            builder.create().show();

                                            break;
                                    }
                                    dialog.cancel();
                                }
                            });
                            secondLevel = sizeBuilder.create();
                            secondLevel.show();

                            break;

                        case 1:
                            dialog.cancel();

                            final AlertDialog secondLevel2;

                            final CharSequence[] speeds = {"slow", "medium", "fast"};

                            AlertDialog.Builder speedBuilder = new AlertDialog.Builder(MasterActivity.this);
                            speedBuilder.setTitle("Options");
                            speedBuilder.setSingleChoiceItems(speeds, -1, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int item) {
                                    MasterActivity.this.playSpeed = item + 2;
                                    dialog.cancel();
                                }
                            });
                            secondLevel2 = speedBuilder.create();
                            secondLevel2.show();

                            break;

                        case 2:
                            /*
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.fg-berlin.eu"));
                            startActivity(intent);
                            */

                            MasterActivity.this.CopyReadAssets();

                            dialog.cancel();
                            break;
                    }
                }
            });
            levelDialog = settingsBuilder.create();
            levelDialog.show();
            return true;
        } else if (id == R.id.action_help) {
            MenuItem play_item = this.menu.findItem(R.id.action_play);
            String name = (String) play_item.getTitle();
            if (name == "Pause") {
                this.onOptionsItemSelected(play_item);
            }

            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.fg-berlin.eu"));
            startActivity(intent);

            return true;
        } else if (id == R.id.action_clear) {
            MenuItem play_item = this.menu.findItem(R.id.action_play);
            String name = (String) play_item.getTitle();
            if (name == "Pause") {
                this.onOptionsItemSelected(play_item);
            }
            this.Master.init(this.Master.H, this.Master.B);

            return true;
        } else if (id == R.id.action_play) {
            String name = (String) item.getTitle();

            if (name.equals("Play")) {
                this.stop = false;

                new Thread() {
                    public void run() {
                        int time = 700 / MasterActivity.this.playSpeed;

                        while (!(MasterActivity.this.hasToStop)) {

                            MasterActivity.this.Master.Step();
                            try {
                                Thread.sleep((long) time);
                            } catch (InterruptedException e) {
                                Thread.currentThread().interrupt();
                            }
                        }
                        MasterActivity.this.stop = true;
                        MasterActivity.this.hasToStop = false;
                    }
                }.start();
                item.setTitle("Pause");
                return true;
            } else {
                this.hasToStop = true;

                while (this.hasToStop || !(this.stop)) ;
                item.setTitle("Play");
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }


    /*
    Cette fonction s'occupe de tout ce qui est "touch", mais son
    geste apparent. Nous l'utilisons exclusivement (à présent)
    pour identifier quand l'utilisateur enlève son doigt (pour
    permettre de changer deux fois de suite la même case) et pour
    identifier des mouvements non spécifiques que fait l'utilisateur
    pour dessiner une confiuration initiale sur l'écran.
     */
    @Override
    public boolean onTouchEvent(MotionEvent event){

        int action = event.getActionMasked();
        switch (action){
            case MotionEvent.ACTION_MOVE:
                int x = (int) event.getX();
                int y = (int) event.getY() - 100;

                if (this.stop) {
                    this.Master.gameView.manipulateGrid(x, y);
                }

                break;

            case MotionEvent.ACTION_UP:
                this.Master.gameView.lastPosition = new Point(-1, -1);
        }

        return true;
    }


    private void CopyReadAssets() {
        /*
        Cette fonction est copiée de la page
        http://stackoverflow.com/questions/12889608/how-to-open-pdf-file-in-android-from-the-assets-folder
        et permet d'ouvrir un fichier .pdf du dossier "assets".
         */
        AssetManager assetManager = getAssets();

        InputStream in = null;
        OutputStream out = null;
        File file = new File(getFilesDir(), "Documentation.pdf");
        try {
            in = assetManager.open("Documentation.pdf");
            out = openFileOutput(file.getName(), Context.MODE_WORLD_READABLE);

            copyFile(in, out);
            in.close();
            in = null;
            out.flush();
            out.close();
            out = null;
        } catch (Exception e) {
            Log.e("tag", e.getMessage());
        }

        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(
                Uri.parse("file://" + getFilesDir() + "/Documentation.pdf"),
                "application/pdf");

        startActivity(intent);
    }

    private void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }
}
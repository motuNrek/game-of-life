

							Projet GameOfLife


Le projet est constitué de deux programmes:

	- un programme qui peut être utilisé sur ordinateur. Ce programme sert de 
		programme type qui nous aide pour le "debugging". Le but final est
		la mise en place d'une application pour le portable (Android).
		
	- l'application.
	

Ces deux programmes sont en partie similaires et en partie très différentes. Tout
d'abord, ils sont tous les deux divisés dans deux sections:
	- le calcul,
	- l'affichage.
	
Le programme sur l'ordinateur comporte encore une classe supplémentaire "Cell.java".
Cette classe n'est cependant pas nécessaire, nous n'avions, à ce stade, simplement
pas encore connaissance de la classe "Point.java" qui est déjà implémentée et qui
peut être utilisée pour notre problème. (Certes, cette classe est beaucoup plus
complexe et prend donc également plus de place dans la mémoire, mais la 
différence est négligeable devant les avantages.)

Les classes du programme pour ordinateur sont complètement commentés. Cependant, la
classe "Calc.class" de l'application n'est presque pas commenté, puisqu'elle est
presque identique à la classe "Master.class" déjà commenté. De même, la classe
"gameView.class" repose en partie sur la classe "Display.class". Cependant, elle
comporte des parties nouveaux, propre à l'utilisation sur portable. La plus grande
différence entre les deux programmes est néanmoins l'apparition de la classe
"MasterActvity.class". Elle est la classe principale de l'application et d'occupe
de la coordination des autres classe. Elle a plusieurs devoirs:

	- lancer l'application, puis contrôler son cycle de vie (interruption,
		reprise, ...)
	- mettre en place le display, surtout en contrôlant la mise en place
		de la classe "gameView.class", mais également le menu qui contient
		les options. Elle s'occupe du layout.
	- mettre en place des classes qui s'occupent des interactions (touch).
	- interactions   utilisateur <-> application.
